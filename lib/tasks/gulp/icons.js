'use strict';

var gulp = require('gulp');

gulp.task('icons', ['clean-icons'], function() {
  return gulp.src('app/assets/icons/*')
    .pipe(gulp.dest('public'));
});
