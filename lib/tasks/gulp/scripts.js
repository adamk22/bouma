'use strict';

// Plugins
var gulp     = require('gulp'),
    jshint   = require('gulp-jshint'),
    uglify   = require('gulp-uglify'),
    rename   = require('gulp-rename'),
    concat   = require('gulp-concat'),
    RevAll   = require('gulp-rev-all'),
    sequence = require("run-sequence");

var jshintFile  = '.jshintrc',
    scripts     = 'app/assets/javascripts/*.js',
    plugins     = 'app/assets/javascripts/plugins/*.js',
    admin       = 'app/assets/javascripts/admin/*.js',
    vendor      = 'app/assets/javascripts/vendor/*.js',
    destination = 'public/assets/javascripts';

gulp.task('scripts', ['clean-scripts'], function(callback) {
  sequence(['scripts-normal', 'scripts-plugins', 'scripts-admin', 'scripts-vendor'], callback);
});

gulp.task('scripts-normal', function() {
  return gulp.src(scripts)
    .pipe(jshint(jshintFile))
    .pipe(jshint.reporter('default'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(destination))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(destination))
});

gulp.task('scripts-plugins', function() {
  return gulp.src(plugins)
    .pipe(concat('scripts-plugins.js'))
    .pipe(gulp.dest(destination))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(destination))
});

gulp.task('scripts-admin', function() {
  return gulp.src(admin)
    .pipe(concat('scripts-admin.js'))
    .pipe(gulp.dest(destination))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(destination))
});

gulp.task('scripts-vendor', function() {
return gulp.src(vendor)
    .pipe(concat('scripts-vendor.js'))
    .pipe(gulp.dest(destination))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest(destination))
});

gulp.task('revision-scripts', function() {
  var revAll = new RevAll();
  gulp.src(['public/assets/javascripts/*'])
    .pipe(revAll.revision())
    .pipe(gulp.dest('public/assets/javascripts'))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest('public/assets/javascripts'))
});
