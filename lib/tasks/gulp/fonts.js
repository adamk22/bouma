'use strict';

// Plugins
var gulp = require('gulp');

// Source paths
var fontsPath = 'app/assets/fonts/**/*';

// Dist paths
var fontsDistPath = 'public/assets/fonts';

// Images
gulp.task('fonts', ['clean-fonts'], function() {
  return gulp.src(fontsPath)
    .pipe(gulp.dest(fontsDistPath));
});
