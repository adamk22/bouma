'use strict';

// Plugins
var gulp  = require('gulp'),
    clean = require('gulp-clean');

// Clean
gulp.task('clean', function () {
  return gulp.src(['public/assets'], {read: false})
    .pipe(clean());
});

gulp.task('clean-styles', function () {
  return gulp.src(['public/assets/stylesheets'], {read: false})
    .pipe(clean());
});

gulp.task('clean-scripts', function () {
  return gulp.src(['public/assets/javascripts'], {read: false})
    .pipe(clean());
});

gulp.task('clean-fonts', function () {
  return gulp.src(['public/assets/fonts'], {read: false})
    .pipe(clean());
});

gulp.task('clean-images', function () {
  return gulp.src(['public/assets/images'], {read: false})
    .pipe(clean());
});

gulp.task('clean-icons', function () {
  return gulp.src([
    'public/*.png',
    'public/*.ico',
    'public/browserconfig.xml',
    'public/manifest.json'
  ], {read: false})
    .pipe(clean());
});
