'use strict';

// Plugins
var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss    = require('gulp-minify-css'),
    rename       = require('gulp-rename'),
    RevAll       = require('gulp-rev-all'),
    sequence     = require("run-sequence"),
    mmq          = require('gulp-merge-media-queries'),
    destination  = 'public/assets/stylesheets';

// Styles
gulp.task('styles', ['clean-styles'], function() {
  gulp.src('app/assets/stylesheets/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(mmq({
      log: false
    }))
    .pipe(gulp.dest(destination))
    .pipe(autoprefixer({
      browsers: ['> 2%', 'last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(destination))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(destination));
});

gulp.task('revision-styles', function() {
  var revAll = new RevAll();
  gulp.src(['public/assets/stylesheets/*'])
    .pipe(revAll.revision())
    .pipe(gulp.dest('public/assets/stylesheets'))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest('public/assets/stylesheets'))
});
