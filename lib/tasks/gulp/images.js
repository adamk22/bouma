'use strict';

var gulp = require('gulp');

gulp.task('images', ['clean-images'], function() {
  return gulp.src('app/assets/images/*')
    .pipe(gulp.dest('public/assets/images'));
});
