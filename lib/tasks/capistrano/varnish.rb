namespace :varnish do
  desc 'Clear Varnish cache'
  task :clear_cache do
    on roles(:app) do
      execute "curl -X PURGE #{fetch(:url)}"
    end
  end

  desc 'Warm Varnish cache'
  task :warm_cache do
    on roles(:app) do
      %w(page blog case employee vacature).each do |item|
        execute "wget --quiet #{fetch(:url)}/#{item}-sitemap.xml --no-cache --output-document - | egrep -o \"https?://$URL[^<]+\" | while read line
              do time curl -A \"Cache Warmer\" -s -L $line
            done"
      end
    end
  end
end
