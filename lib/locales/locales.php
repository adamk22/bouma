<?php

use Symfony\Component\Yaml\Yaml;

class Locales {
  public static $instance;
  private static $translations;
  public $locale = null;

  public static function instance() {
    if (null === static::$instance) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  public function load($path) {

    $translations = [];
    foreach (glob($path) as $file) {
      $locale = pathinfo($file, PATHINFO_FILENAME);
      $locale_translations = Yaml::parse(file_get_contents($file));
      $translations = array_merge_recursive($translations, $locale_translations);
    }

    self::$translations = self::flatten($translations);
    return true;
  }

  public function translate($string, $params = []) {
    $key = "{$this->locale}.{$string}";
    $translation = self::$translations[$key];

    if(!empty($params)) {
      foreach ($params as $key => $value) {
        $translation = str_replace("#{{$key}}", $value, $translation);
      }
    }

    return $translation;
  }

  private static function flatten($array, $prefix = '') {
    $result = array();
    foreach($array as $key=>$value) {
      if(is_array($value)) {
        $result = $result + self::flatten($value, $prefix . $key . '.');
      } else {
        $result[$prefix.$key] = $value;
      }
    }
    return $result;
  }
}

function t($string, $params = []) {
  return Locales::instance()->translate($string, $params);
}
