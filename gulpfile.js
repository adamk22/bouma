// Plugins
var gulp       = require('gulp'),
    requireDir = require('require-dir');
    sequence   = require("run-sequence");

// Source paths
var styles  = 'app/assets/stylesheets/**/*.scss',
    scripts = 'app/assets/javascripts/*.js',
    plugins = 'app/assets/javascripts/plugins/*.js',
    admin   = 'app/assets/javascripts/admin/*.js',
    vendor  = 'app/assets/javascripts/vendor/*.js',
    images  = 'app/assets/images/*',
    fonts   = 'app/assets/fonts/*',
    icons   = 'app/assets/icons/*';

gulp.task('compile', function(callback) {
  sequence([
    'styles',
    'scripts',
    'images',
    'fonts',
    'icons'],
    callback);
});

// Default task
gulp.task('default', ['compile'], function(callback) {
  sequence(['revision-scripts', 'revision-styles'], callback);
});

// Watch
gulp.task('watch', function(callback) {
  // Watch .scss files
  gulp.watch(styles, ['styles']);

  // Watch .js files
  gulp.watch(scripts, ['scripts']);
  gulp.watch(plugins, ['scripts']);
  gulp.watch(admin, ['scripts']);
  gulp.watch(vendor, ['scripts']);

  // Watch image files
  gulp.watch(images, ['images']);

  // Watch font files
  gulp.watch(fonts, ['fonts']);

  // Watch icon files
  gulp.watch(icons, ['icons']);
});

// Theme Tasks
requireDir('lib/tasks/gulp');
