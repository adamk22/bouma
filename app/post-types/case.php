<?php

function case_init() {
  register_post_type( 'case', array(
    'labels'            => array(
      'name'                => __( 'Case', 'bouma' ),
      'singular_name'       => __( 'Case', 'bouma' ),
      'all_items'           => __( 'All Cases', 'bouma' ),
      'new_item'            => __( 'New Case', 'bouma' ),
      'add_new'             => __( 'Add New', 'bouma' ),
      'add_new_item'        => __( 'Add New Case', 'bouma' ),
      'edit_item'           => __( 'Edit Case', 'bouma' ),
      'view_item'           => __( 'View Case', 'bouma' ),
      'search_items'        => __( 'Search Case', 'bouma' ),
      'not_found'           => __( 'No Case found', 'bouma' ),
      'not_found_in_trash'  => __( 'No Case found in trash', 'bouma' ),
      'parent_item_colon'   => __( 'Parent Case', 'bouma' ),
      'menu_name'           => __( 'Case', 'bouma' ),
    ),
    'public'            => true,
    'hierarchical'      => false,
    'show_ui'           => true,
    'show_in_nav_menus' => true,
    'supports'          => array( 'title', 'editor' ),
    'has_archive'       => false,
    'rewrite'           => array( 'slug' => 'cases' ),
    'query_var'         => true,
    'menu_icon'         => 'dashicons-admin-post',
    'show_in_rest'      => true,
    'rest_base'         => 'case',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
  ) );

}
add_action( 'init', 'case_init' );

function case_updated_messages( $messages ) {
  global $post;

  $permalink = get_permalink( $post );

  $messages['case'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Case updated. <a target="_blank" href="%s">View Case</a>', 'bouma'), esc_url( $permalink ) ),
    2 => __('Custom field updated.', 'bouma'),
    3 => __('Custom field deleted.', 'bouma'),
    4 => __('Case updated.', 'bouma'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Case restored to revision from %s', 'bouma'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Case published. <a href="%s">View Case</a>', 'bouma'), esc_url( $permalink ) ),
    7 => __('Case saved.', 'bouma'),
    8 => sprintf( __('Case submitted. <a target="_blank" href="%s">Preview Case</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
    9 => sprintf( __('Case scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Case</a>', 'bouma'),
    // translators: Publish box date format, see http://php.net/date
    date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
    10 => sprintf( __('Case draft updated. <a target="_blank" href="%s">Preview Case</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
  );

  return $messages;
}
add_filter( 'post_updated_messages', 'case_updated_messages' );

//rewrite rule for overview page
function case_rewrite_rules() {
 add_rewrite_rule( '^cases/page/([^/]*)', 'index.php?pagename=cases&paged=$matches[1]', 'top' );
}
add_action( 'init', 'case_rewrite_rules' );