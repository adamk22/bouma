<?php

function weblog_init() {
  register_post_type( 'weblog', array(
    'labels'            => array(
      'name'                => __( 'Weblog', 'bouma' ),
      'singular_name'       => __( 'Weblog', 'bouma' ),
      'all_items'           => __( 'All Weblogs', 'bouma' ),
      'new_item'            => __( 'New Weblog', 'bouma' ),
      'add_new'             => __( 'Add New', 'bouma' ),
      'add_new_item'        => __( 'Add New Weblog', 'bouma' ),
      'edit_item'           => __( 'Edit Weblog', 'bouma' ),
      'view_item'           => __( 'View Weblog', 'bouma' ),
      'search_items'        => __( 'Search Weblog', 'bouma' ),
      'not_found'           => __( 'No Weblog found', 'bouma' ),
      'not_found_in_trash'  => __( 'No Weblog found in trash', 'bouma' ),
      'parent_item_colon'   => __( 'Parent Weblog', 'bouma' ),
      'menu_name'           => __( 'Weblog', 'bouma' ),
    ),
    'public'            => true,
    'hierarchical'      => false,
    'show_ui'           => true,
    'show_in_nav_menus' => true,
    'supports'          => array( 'title', 'editor' ),
    'has_archive'       => false,
    'rewrite'           => array( 'slug' => 'weblog' ),
    'query_var'         => true,
    'menu_icon'         => 'dashicons-admin-post',
    'show_in_rest'      => true,
    'rest_base'         => 'weblog',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
  ) );

}
add_action( 'init', 'weblog_init' );

function weblog_updated_messages( $messages ) {
  global $post;

  $permalink = get_permalink( $post );

  $messages['weblog'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Weblog updated. <a target="_blank" href="%s">View Weblog</a>', 'bouma'), esc_url( $permalink ) ),
    2 => __('Custom field updated.', 'bouma'),
    3 => __('Custom field deleted.', 'bouma'),
    4 => __('Weblog updated.', 'bouma'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Weblog restored to revision from %s', 'bouma'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Weblog published. <a href="%s">View Weblog</a>', 'bouma'), esc_url( $permalink ) ),
    7 => __('Weblog saved.', 'bouma'),
    8 => sprintf( __('Weblog submitted. <a target="_blank" href="%s">Preview Weblog</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
    9 => sprintf( __('Weblog scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Weblog</a>', 'bouma'),
    // translators: Publish box date format, see http://php.net/date
    date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
    10 => sprintf( __('Weblog draft updated. <a target="_blank" href="%s">Preview Weblog</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
  );

  return $messages;
}
add_filter( 'post_updated_messages', 'weblog_updated_messages' );

//rewrite rule for overview page
function weblog_rewrite_rules() {
 add_rewrite_rule( '^weblogs/page/([^/]*)', 'index.php?pagename=weblogs&paged=$matches[1]', 'top' );
}
add_action( 'init', 'weblog_rewrite_rules' );