<?php

// function bouma_init() {
//   $labels = [
//     'name'               => t( 'bouma.labels.name' ),
//     'singular_name'      => t( 'bouma.labels.singular_name' ),
//     'all_items'          => t( 'bouma.labels.all_items' ),
//     'new_item'           => t( 'bouma.labels.new_item' ),
//     'add_new'            => t( 'bouma.labels.add_new' ),
//     'add_new_item'       => t( 'bouma.labels.add_new_item' ),
//     'edit_item'          => t( 'bouma.labels.edit_item' ),
//     'view_item'          => t( 'bouma.labels.view_item' ),
//     'search_items'       => t( 'bouma.labels.search_items' ),
//     'not_found'          => t( 'bouma.labels.not_found' ),
//     'not_found_in_trash' => t( 'bouma.labels.not_found_in_trash' ),
//     'menu_name'          => t( 'bouma.labels.menu_name' ),
//   ];

//   $args = [
//     'labels'            => $labels,
//     'public'            => true,
//     'hierarchical'      => false,
//     'show_ui'           => true,
//     'show_in_nav_menus' => true,
//     'supports'          => [ 'title', 'editor', 'revisions' ],
//     'has_archive'       => false,
//     'rewrite'           => [ 'slug' => 'bouma' ],
//     'query_var'         => true,
//     'menu_icon'         => 'dashicons-media-document',
//   ];

//   register_post_type( 'bouma', $args );
// }
// add_action( 'init', 'bouma_init' );

// function bouma_updated_messages( $messages ) {
//   global $post;

//   $permalink = get_permalink( $post );

//   $messages['bouma'] = [
//     1  => t( 'bouma.messages.updated', [ 'permalink' => esc_url( $permalink ) ] ),
//     2  => t( 'bouma.messages.custom_field_updated' ),
//     3  => t( 'bouma.messages.custom_field_deleted' ),
//     4  => t( 'bouma.messages.updated', [ 'permalink' => esc_url( $permalink ) ] ),
//     5  => isset( $_GET['revision'] ) ? t( 'bouma.messages.restored_from_revision', [ 'revision', esc_url( $_GET['revision'] ) ] ) : false,
//     6  => t( 'bouma.messages.published', [ 'permalink' => esc_url( $permalink ) ] ),
//     7  => t( 'bouma.messages.saved' ),
//     8  => t( 'bouma.messages.submitted', [ 'permalink', esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ] ),
//     9  => t( 'bouma.messages.scheduled_for', [ 'date' => date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ), 'permalink' => esc_url( $permalink ) ] ),
//     10 => t( 'bouma.messages.draft_updated', [ 'permalink' => esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ] ),
//   ];

//   return $messages;
// }
// add_filter( 'post_updated_messages', 'bouma_updated_messages' );
