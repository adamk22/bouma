<?php

function Medewerker_init() {
  register_post_type( 'Medewerker', array(
    'labels'            => array(
      'name'                => __( 'Medewerker', 'bouma' ),
      'singular_name'       => __( 'Medewerker', 'bouma' ),
      'all_items'           => __( 'All Medewerkers', 'bouma' ),
      'new_item'            => __( 'New Medewerker', 'bouma' ),
      'add_new'             => __( 'Add New', 'bouma' ),
      'add_new_item'        => __( 'Add New Medewerker', 'bouma' ),
      'edit_item'           => __( 'Edit Medewerker', 'bouma' ),
      'view_item'           => __( 'View Medewerker', 'bouma' ),
      'search_items'        => __( 'Search Medewerker', 'bouma' ),
      'not_found'           => __( 'No Medewerker found', 'bouma' ),
      'not_found_in_trash'  => __( 'No Medewerker found in trash', 'bouma' ),
      'parent_item_colon'   => __( 'Parent Medewerker', 'bouma' ),
      'menu_name'           => __( 'Medewerker', 'bouma' ),
    ),
    'public'            => true,
    'hierarchical'      => false,
    'show_ui'           => true,
    'show_in_nav_menus' => true,
    'supports'          => array( 'title', 'editor' ),
    'has_archive'       => true,
    'rewrite'           => array( 'slug' => 'medewerker' ),
    'query_var'         => true,
    'menu_icon'         => 'dashicons-admin-post',
    'show_in_rest'      => true,
    'rest_base'         => 'Medewerker',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
  ) );

}
add_action( 'init', 'Medewerker_init' );

function Medewerker_updated_messages( $messages ) {
  global $post;

  $permalink = get_permalink( $post );

  $messages['Medewerker'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Medewerker updated. <a target="_blank" href="%s">View Medewerker</a>', 'bouma'), esc_url( $permalink ) ),
    2 => __('Custom field updated.', 'bouma'),
    3 => __('Custom field deleted.', 'bouma'),
    4 => __('Medewerker updated.', 'bouma'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Medewerker restored to revision from %s', 'bouma'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Medewerker published. <a href="%s">View Medewerker</a>', 'bouma'), esc_url( $permalink ) ),
    7 => __('Medewerker saved.', 'bouma'),
    8 => sprintf( __('Medewerker submitted. <a target="_blank" href="%s">Preview Medewerker</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
    9 => sprintf( __('Medewerker scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Medewerker</a>', 'bouma'),
    // translators: Publish box date format, see http://php.net/date
    date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
    10 => sprintf( __('Medewerker draft updated. <a target="_blank" href="%s">Preview Medewerker</a>', 'bouma'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
  );

  return $messages;
}
add_filter( 'post_updated_messages', 'Medewerker_updated_messages' );