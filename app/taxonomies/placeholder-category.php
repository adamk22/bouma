<?php
// function bouma_category_init() {

//   $labels = [
//     'name'                       => t('bouma_category.labels.name'),
//     'singular_name'              => t('bouma_category.labels.singular_name'),
//     'search_items'               => t('bouma_category.labels.search_items'),
//     'popular_items'              => t('bouma_category.labels.popular_items'),
//     'all_items'                  => t('bouma_category.labels.all_items'),
//     'parent_item'                => t('bouma_category.labels.parent_item'),
//     'parent_item_colon'          => t('bouma_category.labels.parent_item_colon'),
//     'edit_item'                  => t('bouma_category.labels.edit_item'),
//     'update_item'                => t('bouma_category.labels.update_item'),
//     'add_new_item'               => t('bouma_category.labels.add_new_item'),
//     'new_item_name'              => t('bouma_category.labels.new_item_name'),
//     'separate_items_with_commas' => t('bouma_category.labels.separate_items_with_commas'),
//     'add_or_remove_items'        => t('bouma_category.labels.add_or_remove_items'),
//     'choose_from_most_used'      => t('bouma_category.labels.choose_from_most_used'),
//     'menu_name'                  => t('bouma_category.labels.menu_name'),
//   ];

//   $args = [
//     'labels'            => $labels,
//     'hierarchical'      => false,
//     'public'            => true,
//     'show_in_nav_menus' => true,
//     'show_ui'           => true,
//     'show_admin_column' => true,
//     'query_var'         => true,
//     'rewrite'           => [ 'slug' => 'bouma/tag' ],
//     'capabilities' => [
//       'manage_terms' => 'manage_categories',
//       'edit_terms'   => 'manage_categories',
//       'delete_terms' => 'manage_categories',
//       'assign_terms' => 'manage_categories',
//     ],
//   ];

//   register_taxonomy( 'bouma_category', [ 'bouma' ], $args );
// }
// add_action( 'init', 'bouma_category_init' );
