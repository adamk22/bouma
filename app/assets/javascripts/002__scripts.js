// ==========================================================================
// Mobile menu handling
// ==========================================================================

$(document).ready(function() {
  var scroll_pos = $(window).scrollTop(),
      header_height = $('header').height() - 200,
      mobile_menu = $('#mobile-menu'),
      mobile_overlay = $('#mobile-menu-overlay');

  if(scroll_pos > header_height) {
    $('#mobile-menu').addClass('black-icon').removeClass('white-icon');
  } else {
    $('#mobile-menu').removeClass('black-icon').addClass('white-icon');
  }

  $('#mobile-menu').on('click', function() {
    var open_close_fixup = function() {
      if(mobile_overlay.hasClass('menu-closed')) {
        mobile_overlay.removeClass('menu-closed').addClass('menu-open');
        $('html, body').addClass('no-scroll');
      } else {
        mobile_overlay.addClass('menu-closed').removeClass('menu-open');
        $('html, body').removeClass('no-scroll');
      }
    };

    open_close_fixup();

    var open = false;
    if (mobile_overlay.hasClass('menu-open')) {
      open = true;
    }

    if (open === true) {
      mobile_menu.removeClass('black-icon').addClass('white-icon');
      mobile_menu.removeClass('icon-menu').addClass('icon-cross');
    } else {
      mobile_menu.removeClass('black-icon').addClass('white-icon');
      mobile_menu.addClass('icon-menu').removeClass('icon-cross');
    }
  });
});

// ==========================================================================
// Mobile menu icon color transition
// ==========================================================================

$(document).ready(function() {
    var scroll_pos = 0,
        header_height = $('header').height() - 200,
        footer_pos = $('footer').offset().top - 50,
        menu_icon = $(".menu-main-icon");

    $(window).scroll(function() {
    scroll_pos = $(this).scrollTop();

    if(scroll_pos > header_height && scroll_pos < footer_pos) {
      menu_icon.addClass('black-icon');
      menu_icon.removeClass('white-icon');

    } else if(scroll_pos > footer_pos) {
      menu_icon.removeClass('black-icon');
      menu_icon.addClass('white-icon');

    } else {
      menu_icon.removeClass('black-icon');
      menu_icon.addClass('white-icon');
    }
  });
});

// ==========================================================================
// Sticky menu
// ==========================================================================

$(document).ready(function() {
  var didScroll,
      lastScrollTop = 0,
      delta = 5,
      menu_wrapper = $('.menu-wrapper'),
      navbarHeight = menu_wrapper.outerHeight(),
      top_menu_height = $('.menu-top-wrapper').outerHeight();

  // on scroll, let the interval function know the user has scrolled
  $(window).scroll(function(event){
    didScroll = true;
    if($(window).scrollTop() === 0) {
      menu_wrapper.addClass('top-nav-difference');
    } else {
      menu_wrapper.removeClass('top-nav-difference');
    }
  });

  // On load and refresh check if the navbar is on top of the document
  // If so, add class.
  if(menu_wrapper.offset().top <= 50) {
    menu_wrapper.addClass('top-nav-difference');
  } else {
    menu_wrapper.removeClass('top-nav-difference');
  }

  function hasScrolled() {
    var st = $(this).scrollTop();
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta) {
      return;
    }
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
      // Scroll Down
      menu_wrapper.removeClass('nav-down').addClass('nav-up');
    } else {
      // Scroll Up
      if(st + $(window).height() < $(document).height()) {
        menu_wrapper.removeClass('nav-up').addClass('nav-down');
      }
    }
    lastScrollTop = st;
  }

  // run hasScrolled() and reset didScroll status
  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 10);

  // on scroll, let the interval function know the user has scrolled
  $(window).scroll(function(event){
    didScroll = true;
  });

  // run hasScrolled() and reset didScroll status
  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 10);
});

// ==========================================================================
// IE & Safari Detection
// ==========================================================================

$(document).ready(function() {
  function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    // other browser
    return false;
  }

  var versionofIE = detectIE();
  //Check if browser is IE
  if (versionofIE !== false) {
    $('body').addClass('no-mix-blend ie-browser');
  }

  // Check if browser is Safari
  if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) {
    $('body').addClass('no-mix-blend safari-browser');
  }
});

// ==========================================================================
// Fitvid Init
// ==========================================================================

$(document).ready(function(){
  $(".weblog-video").fitVids();
});

// ==========================================================================
// Maps Switcher
// ==========================================================================

$(document).ready(function() {
  $('.maps-arnhem').on('click', function() {
    $('#rotterdam').removeClass('show-map').addClass('hide-map');
    $('#arnhem').removeClass('hide-map').addClass('show-map');
    $('.maps-rotterdam').removeClass('active-item');
    $(this).addClass('active-item');
  });

  $('.maps-rotterdam').on('click', function() {
    $('#arnhem').removeClass('show-map').addClass('hide-map');
    $('#rotterdam').removeClass('hide-map').addClass('show-map');
    $('.maps-arnhem').removeClass('active-item');
    $(this).addClass('active-item');
  });
});
