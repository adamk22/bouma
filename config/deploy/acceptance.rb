set :stage, :acceptance

# Used for Varnish cache clearing
set :url, Settings.stages.acceptance.url

# deploy to
server Settings.stages.acceptance.server, user: 'deployer', roles: %w(web app db), primary: true
set :deploy_to, Settings.stages.acceptance.path

# debug
set :log_level, :info
set :composer_install_flags, '--no-dev --no-interaction'

# merge configurations
fetch(:default_env).merge!(wp_env: :acceptance)
