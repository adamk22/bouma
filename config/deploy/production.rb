set :stage, :production

# Used for Varnish cache clearing
set :url, Settings.stages.production.url

# deploy to
server Settings.stages.production.server, user: 'deployer', roles: %w(web app db), primary: true
set :deploy_to, Settings.stages.production.path

# debug
set :log_level, :info
set :composer_install_flags, '--no-dev --no-interaction --optimize-autoloader'

# merge configurations
fetch(:default_env).merge!(wp_env: :production)
