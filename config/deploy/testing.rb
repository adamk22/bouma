set :stage, :testing

# Used for Varnish cache clearing
set :url, Settings.stages.testing.url

# deploy to
server Settings.stages.testing.server, user: 'deployer', roles: %w(web app db), primary: true
set :deploy_to, Settings.stages.testing.path

# debug
set :log_level, :info
set :composer_install_flags, '--optimize-autoloader'

# merge configurations
fetch(:default_env).merge!(wp_env: :testing)
