# Capistrano configuration for all deployment stages
# Put stage specific configuration in deploy/stage_name.rb
set :application, Settings.application_name
set :repo_url, Settings.git_repo_url

# Branch options
# Prompts for the branch name (defaults to current branch)
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# deployment directory
set :deploy_to, "/var/www/#{fetch(:deploy_to)}"

# Debug levels
set :log_level, :info

# rbenv
set :rbenv_type, :user
set :rbenv_ruby, '2.2.3'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"

# npm
set :npm_flags, '--production --silent'

# Whenever crontab
set :whenever_identifier, "#{fetch(:application)}_#{fetch(:stage)}"

set :linked_files, %w(.env config/salts.php)
set :linked_dirs, %w(node_modules vendor public/uploads public/ewww)

after 'deploy:finishing', 'gulp'
after 'deploy:finishing', 'wp:fix_site_transient_theme_roots'
