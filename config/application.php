<?php

# Let other plugins and themes know bedrock structure is being used
define( 'BEDROCK', true );

# load .env file if it exists
# Apache / nginex maybe set these Env variables also
if ( file_exists( ROOTPATH . '/.env' ) ) {
  Dotenv::load( ROOTPATH );
}

Dotenv::required(
  array( 'DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_ENV', 'WP_HOME' )
);

# Database constants taken from the .env or Env variables
define( 'DB_NAME', getenv( 'DB_NAME' ) );
define( 'DB_USER', getenv( 'DB_USER' ) );
define( 'DB_PASSWORD', getenv( 'DB_PASSWORD' ) );
define( 'DB_HOST', getenv( 'DB_HOST' ) ? getenv( 'DB_HOST' ) : 'localhost' );

# WordPress environment
define( 'WP_ENV', getenv( 'WP_ENV' ) ? getenv( 'WP_ENV' ) : 'development' );

# require the environment specific config
$env_config = ROOTPATH . '/config/environments/' . WP_ENV . '.php';

if ( file_exists( $env_config ) ) {
  require $env_config;
}

# Environments for wp-stage-switcher
# https://github.com/roots/wp-stage-switcher
$envs = array(
  'development' => 'http://bouma.dev',
  'testing'     => 'http://test.bouma.nl',
  'acceptance'  => 'http://acceptatie.bouma.nl',
  'production'  => 'http://www.bouma.nl',
);
define( 'ENVIRONMENTS', serialize( $envs ) );

# Load the salts for this environment
# https://api.wordpress.org/secret-key/1.1/salt/
if ( file_exists( ROOTPATH . '/config/salts.php' ) ) {
  require ROOTPATH . '/config/salts.php';
}

# Database Charset to use in creating database tables.
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

# Default admin language
define( 'WPLANG', '' );
define( 'WP_LANG_DIR', ROOTPATH . '/public/wp/wp-content/languages' );

# Protocol: http or https
# Change this by setting the PROTOCOL Env variable
define( 'PROTOCOL', getenv( 'PROTOCOL' ) ? getenv( 'PROTOCOL' ) : 'http' );
# This is for when nginx is decrypting ssl for varnish or apache.
# the header should be set in nginx like this :
# proxy_set_header X-Forwarded-Proto https;
# proxy_set_header X-Forwarded-Port 443;
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) {
  $_SERVER['HTTPS'] = 'on';
}

# Domain
define( 'WP_HOME', PROTOCOL . '://' . getenv( 'WP_HOME' )  );
define( 'DOMAIN_CURRENT_SITE', WP_HOME );
define( 'WP_SITEURL', WP_HOME . '/wp' );

define( 'JAVASCRIPTS_BASE_URL', WP_HOME . '/assets/javascripts/');
define( 'STYLESHEETS_BASE_URL', WP_HOME . '/assets/stylesheets/');

# Custom Content Directory
define( 'WP_CONTENT_DIR', ROOTPATH . '/public' );
define( 'WP_CONTENT_URL', WP_HOME );

# Disable automatic updates
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );

# Disable this poor mans cron if you want, but remember posts scheduling
define( 'DISABLE_WP_CRON', false );

# Revisions
define( 'WP_POST_REVISIONS', 10 );

# Mouseflow
define( 'MOUSEFLOW_ID', getenv( 'MOUSEFLOW_ID' ) );

# Google Analytics
define( 'GA_ID', getenv( 'GA_ID' ) );

# WordPress Database Table prefix.
$table_prefix  = 'wp_';

# Absolute path to the WordPress directory.
if ( ! defined( 'ABSPATH' ) ) {
  define( 'ABSPATH', ROOTPATH . '/public/wp' );
}

/* Multisite */
//define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
// define('DOMAIN_CURRENT_SITE', 'bouma.dev');
// define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

# Google Analytics
define( 'GOOGLE_ANALYTICS_CODE', getenv( 'GOOGLE_ANALYTICS_CODE' ) );