<?php
# Template Name: Weblog overzicht
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
  'post_type'         => 'weblog',
  'post_status'       => 'publish',
  'paged'             => $paged,
  'posts_per_page'    =>  10,
  'orderby'           => 'post_date',
  'order'             => 'DESC',
);
query_posts($args);

$context = Timber::get_context();
$context['post']        = new TimberPost();
$context['weblog_items']  = Timber::get_posts($args);
$context['paged']       = $paged;
$context['pagination']  = Timber::get_pagination();
$context['breadcrumb'] = bouma_get_breadcrumb();

Timber::render( [ 'page-weblogs.twig' ], $context );
