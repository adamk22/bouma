<?php
# Template Name: Octrooien
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['breadcrumb'] = bouma_get_breadcrumb();

Timber::render( [ 'page-octrooien.twig' ], $context );
