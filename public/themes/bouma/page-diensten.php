<?php
# Template Name: Diensten
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['breadcrumb'] = bouma_get_breadcrumb();

Timber::render( [ 'page-diensten.twig' ], $context );
