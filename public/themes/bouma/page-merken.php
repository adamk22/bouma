<?php
# Template Name: Merken
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['breadcrumb'] = bouma_get_breadcrumb();

Timber::render( [ 'page-merken.twig' ], $context );
