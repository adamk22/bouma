<?php

$args = array(
  'name'        => 'error',
  'post_type'   => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$posts = get_posts($args);
if (empty($posts[0]->ID)) {
  die('No 404 page defined yet!');
}

$post = new TimberPost($posts[0]->ID);
$context = Timber::get_context();
$context['post'] = $post;
$context['breadcrumb'] = bouma_get_breadcrumb();
Timber::render( '404.twig', $context );