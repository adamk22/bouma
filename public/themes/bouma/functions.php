<?php

if ( ! class_exists( 'Timber' ) ) {
  add_action( 'admin_notices', function() {
      echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
  } );
  return;
}

function bouma_enqueue_styles() {
  $minified = in_array( WP_ENV, [ 'acceptance', 'production' ] ) ? '.min' : '';

  wp_enqueue_style( 'stylesheet', STYLESHEETS_BASE_URL . gulp_stylesheet("style{$minified}.css"), false, null );
}
add_action( 'wp_enqueue_scripts', 'bouma_enqueue_styles' );

function gulp_stylesheet_manifest() {
  global $gulp_stylesheet_manifest;
  if (empty($gulp_stylesheet_manifest)) {
    $content = file_get_contents(ROOTPATH . '/public/assets/stylesheets/rev-manifest.json');
    $gulp_stylesheet_manifest = json_decode($content, true);
  }

  return $gulp_stylesheet_manifest;
}

function gulp_stylesheet($file) {
  if (WP_ENV == 'development') return $file;

  $gulp_stylesheet_manifest = gulp_stylesheet_manifest();

  return $gulp_stylesheet_manifest[$file];
}

function gulp_javascript_manifest() {
  global $gulp_javascript_manifest;
  if (empty($gulp_javascript_manifest)) {
    $content = file_get_contents(ROOTPATH . '/public/assets/javascripts/rev-manifest.json');
    $gulp_javascript_manifest = json_decode($content, true);
  }

  return $gulp_javascript_manifest;
}

function gulp_javascript($file) {
  if (WP_ENV == 'development') return $file;

  $gulp_javascript_manifest = gulp_javascript_manifest();

  return $gulp_javascript_manifest[$file];
}

function bouma_enqueue_scripts() {
  $minified = in_array( WP_ENV, [ 'acceptance', 'production' ] ) ? '.min' : '';

  // Deregister WordPress core jQuery
  if ( ! is_admin() ) { wp_deregister_script( 'jquery' ); }

  // Register scripts-vendor as jquery
  // External plugins depending on jquery will be enqueued after our vendor script
  wp_enqueue_script( 'jquery', JAVASCRIPTS_BASE_URL . gulp_javascript( "scripts-vendor{$minified}.js" ), [], null, false );
  wp_enqueue_script( 'script', JAVASCRIPTS_BASE_URL . gulp_javascript( "scripts{$minified}.js" ), [], null, true );
}
add_action( 'wp_enqueue_scripts', 'bouma_enqueue_scripts' );


# Only call this method from method/function that hooks action: after_switch_theme
# Otherwise you might be adding these on every request!
function bouma_add_image_size( $name, $width = 0, $height = 0, $crop = false ) {
  update_option( "{$name}_size_w", $width, '', 'yes' );
  update_option( "{$name}_size_h", $height, '', 'yes' );
  update_option( "{$name}_crop", ( (int) $crop ), '', 'yes' );
}

function bouma_add_image_sizes_to_db() {
  // bouma_add_image_size( slug:string, width:int, height:int, crop:boolean );
  jf_add_image_size( 'quote', 750, 750, true );
  jf_add_image_size( 'employee', 300, 360, true );
}
add_action( 'after_switch_theme', 'bouma_add_image_sizes_to_db' );

function bouma_add_image_sizes() {
  // add_image_size( slug:string, width:int, height:int, crop:boolean );
  add_image_size( 'quote', 750, 750, true );
  add_image_size( 'employee', 300, 360, true );
}
add_filter( 'init', 'bouma_add_image_sizes' );


// Register footer and main menu
function register_menus() {
  register_nav_menus( array(
      'top'   => 'Top',
      'main'   => 'Main',
      'footer' => 'Footer',
  ));
}
add_filter( 'init', 'register_menus' );

// Remove generator meta element
function rm_generator_filter() {
  return '';
}
add_filter( 'the_generator', 'rm_generator_filter' );

// Remove wlwmanifest and rsd link elements
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
// Remove the embed on the frontend
remove_action('wp_head', 'wp_oembed_add_host_js');


// Change the logo used on the wp-admin page from WP to custom logo
function bouma_login_logo() {
  ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
      background: transparent url(/assets/images/login-logo.png) no-repeat 50% 50%;
      width: 205px;
      height: 38px;
    }
  </style>
  <?php
}
add_action( 'login_enqueue_scripts', 'bouma_login_logo' );


function bouma_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', 'bouma_login_logo_url' );


function bouma_remove_thumbnail_box() {
  remove_meta_box( 'postimagediv', 'page', 'side' );
}
add_action( 'do_meta_boxes', 'bouma_remove_thumbnail_box' );


Timber::$dirname = array( 'templates', 'views' );

class PlaceholderSite extends TimberSite {

  function __construct() {
    add_theme_support( 'post-formats' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'menus' );
    add_filter( 'timber_context', array( $this, 'add_to_context' ) );
    add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
    parent::__construct();
  }

  function add_to_context( $context ) {
    $context['top_menu'] = new TimberMenu( 'top' );
    $context['main_menu'] = new TimberMenu( 'main' );
    $context['footer_menu'] = new TimberMenu( 'footer' );
    $context['site'] = $this;

    return $context;
  }

  function add_to_twig( $twig ) {
    $twig->addExtension( new Twig_Extension_StringLoader() );

    return $twig;
  }
}

// TODO: also within class above -> remove this one?
function add_to_twig( $twig ) {
  /* this is where you can add your own fuctions to twig */
  $twig->addExtension( new Twig_Extension_StringLoader() );

  return $twig;
}
add_filter( 'get_twig', 'add_to_twig' );


if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page();
  // acf_add_options_sub_page( name:string );
}

new PlaceholderSite();

function bouma_get_breadcrumb() {
  if (!function_exists('yoast_breadcrumb')) {
    return;
  }

  $yoast_breadcrumb = yoast_breadcrumb('', '', false);
  if (empty($yoast_breadcrumb)) {
    return;
  }

  $pieces = explode(',', $yoast_breadcrumb);
  foreach ($pieces as $k => &$piece) {
    if ($k == 0) {
      $piece = '<a href="/"><li class="breadcrumb-list__item breadcrumb__house"><span class="icon-house"></span></li></a>';
    }
    else {
      $piece = '<li class="breadcrumb-list__item breadcrumb__text">' . $piece . '</li>';
    }
  }

  // print '<pre>'; print_r($pieces); print '</pre>';

  global $post;
  $blog_id = get_current_blog_id();
  $overview_pages = array(
    'cases' => array(
      1 => array('name' => 'Cases', 'url' => '/cases'),
      2 => array('name' => 'Cases', 'url' => '/en/cases'),
    ),
    'weblog' => array(
      1 => array('name' => 'Weblogs', 'url' => '/weblogs'),
      2 => array('name' => 'Weblogs', 'url' => '/en/weblogs'),
    ),
  );

  if ($post->post_type == 'case') {
    $pieces[2] = $pieces[1];
    $pieces[1] = '<li class="breadcrumb-list__item breadcrumb__text"><a href="' . $overview_pages['cases'][$blog_id]['url'] . '">' . $overview_pages['cases'][$blog_id]['name'] . '</a></li>';
  }
  else if ($post->post_type == 'weblog') {
    $pieces[1] = '<li class="breadcrumb-list__item breadcrumb__text"><a href="' . $overview_pages['weblog'][$blog_id]['url'] . '">' . $overview_pages['weblog'][$blog_id]['name'] . '</a></li>';
  }

  return implode('<li class="breadcrumb-list__item icon-caret-right breadcrumb__arrow"></li>', $pieces);
}

function bouma_body_class( $classes ) {
  global $post;

  if (empty($post->post_title)) {
    return $classes;
  }

  $classes[] = 'page-'.sanitize_title($post->post_title);
  return $classes;
}
add_filter( 'body_class', 'bouma_body_class' );

function bouma_get_google_analytics_code() {
  return GOOGLE_ANALYTICS_CODE;
}
