<?php

defined( 'ABSPATH' ) || exit;

function bouma_acfwpcli_fieldgroup_paths( $paths ) {
  $paths['bouma'] = ROOTPATH . '/app/field-groups/';
  return $paths;
}
add_filter( 'acfwpcli_fieldgroup_paths', 'bouma_acfwpcli_fieldgroup_paths' );

function bouma_require_files() {
  foreach ( array( 'post-types', 'taxonomies' ) as $type ) {
    foreach ( glob( ROOTPATH . "/app/$type/*.php" ) as $filename ) {
      require $filename;
    }
  }
}
add_action( 'muplugins_loaded', 'bouma_require_files' );

function bouma_load_translations() {
  $locales = Locales::instance();
  $locales->load(ROOTPATH . '/config/locales/*.yml');
  $locales->locale = get_locale();
}
add_action('init', 'bouma_load_translations');
