<?php

defined( 'ABSPATH' ) || exit;

# The root of this project
# Use to include files easily from anywhere in the project
define( 'ROOTPATH', str_replace( '/public', '', dirname( __FILE__ ) ) );

# Load Composer packages
require ROOTPATH . '/vendor/autoload.php';

# Require library for translations
require ROOTPATH . '/lib/locales/locales.php';

# Load application configuration
require ROOTPATH . '/config/application.php';

# Continue loading WordPress
require ROOTPATH . '/public/wp/wp-settings.php';
